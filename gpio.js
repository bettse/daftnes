var onoff = require('onoff'); //#A

var Gpio = onoff.Gpio;
var led = new Gpio(18, 'out');
var reset = new Gpio(24, 'in', 'both');
var power = new Gpio(23, 'in', 'both');

reset.watch(function(err, value) {
  led.write(value, function() {
    console.log("Changed LED state to: " + value);
  });
});


power.watch(function(err, value) {
  led.write(value, function() {
    console.log("Changed LED state to: " + value);
  });
});


process.on('SIGINT', function () {
  led.writeSync(0);
  led.unexport();
  console.log('Bye, bye!');
  process.exit();
});

