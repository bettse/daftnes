const PN532 = require('pn532').PN532;
const SerialPort = require('serialport');
const ndef = require('ndef');

const tagTimeoutMs = 1500;

class Cartridge extends PN532 {
  constructor() {
    const serialPort = new SerialPort('/dev/serial0', { baudrate: 115200 });
    super(serialPort);
    this.currentTag = null;
    this.tagTimer;
    this.on('ready', this.rfidReady);
  }

  rfidReady() {
    this.on('tag', this.newTag);
  }

  newTag(tag) {
    // Detect tag removal by timing out tag detection
    clearTimeout(this.tagTimer);
    this.tagTimer = setTimeout(() => {
      this.currentTag = null;
      this.emit('eject');
    }, tagTimeoutMs);

    // Prevent repeated parsing of the same tag
    if(this.currentTag && this.currentTag.uid == tag.uid) {
      return;
    }
    this.currentTag = tag;

    this.readNdefData().then((data) => {
      // Detect tag removal by timing out tag detection
      var records = ndef.decodeMessage(Array.from(data));
      this.emit('insert', records);
    });
  }

}

module.exports = Cartridge;
