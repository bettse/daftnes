var ndef = require('ndef');
var fs = require('fs');
var Cartridge = require('./cartridge.js');
var onoff = require('onoff');
var ps = require('ps-node');
var fkill = require('fkill');
var spawn = require('child_process').spawn;
var Gpio = onoff.Gpio;

const emulators = [
  "amiga", "amstradcpc", "apple2", "arcade", "atari800", "atari2600", "atari5200", "atari7800",
  "atarilynx", "atarist", "c64", "coco", "dragon32", "dreamcast", "fba", "fds", "gamegear", "gb", "gba",
  "gbc", "intellivision", "macintosh", "mame-advmame", "mame-libretro", "mame-mame4all", "mastersystem",
  "megadrive", "msx", "n64", "neogeo", "nes", "ngp", "ngpc", "pc", "ports", "psp", "psx", "scummvm",
  "sega32x", "segacd", "sg-1000", "snes", "vectrex", "videopac", "wonderswan", "wonderswancolor",
  "zmachine", "zxspectrum"
];
const procnames = [
  "retroarch", "ags", "uae4all2", "uae4arm", "capricerpi", "linapple", "hatari", "stella",
  "atari800", "xroar", "vice", "daphne", "reicast", "pifba", "osmose", "gpsp", "jzintv",
  "basiliskll", "mame", "advmame", "dgen", "openmsx", "mupen64plus", "gngeo", "dosbox", "ppsspp",
  "simcoupe", "scummvm", "snes9x", "pisnes", "frotz", "fbzx", "fuse", "gemrb", "cgenesis", "zdoom",
  "eduke32", "lincity", "love", "alephone", "micropolis", "openbor", "openttd", "opentyrian",
  "cannonball", "tyrquake", "ioquake3", "residualvm", "xrick", "sdlpop", "uqm", "stratagus",
  "wolf4sdl", "solarus", "emulationstation", "emulationstatio"
];

const led = new Gpio(18, 'out');
const power = new Gpio(23, 'in', 'both');
const reset = new Gpio(24, 'in', 'both');
const debounce = 20;
const doubleClickGap = 250;
const hold = 1000;
const longHold = 2500;

let cartridge = new Cartridge();
var cartok = false;
var clickToggle = false;
var resetStart;

process.on('SIGINT', function () {
  led.unexport();
  power.unexport();
  reset.unexport();
});

cartridge.on('insert', insert);
cartridge.on('eject', eject);

power.watch(function(err, value) {
  if (err) {
    throw err;
  }
  console.log("POWER", value);
  if (value) {
    buttonOn();
  } else {
    buttonOff();
  }
});

// Need to handle different reset hold lengths
reset.watch(function(err, value) {
  if (err) {
    throw err;
  }
  console.log("RESET", value);
  if (value) {
    resetStart = new Date();
  } else if (resetStart) {
    var ms = new Date() - resetStart;
    resetStart = null;
    console.log('held', ms, 'ms');

    if (ms < debounce) {
      // Ignore
    } else if (ms < hold) {
      // Push
      if (clickToggle) {
        // Double click
        ledToggle();
        clickToggle = false;
      } else {
        clickToggle = true;
        setTimeout(() => {
          if (clickToggle) { // If doubleclick hasn't reset it
            // Single click
            clickToggle = false;
            nesReset();
          }
        }, doubleClickGap);
      }
    } else if (ms < longHold) {
      // Hold
      powerOn();
    } else {
      // Long Hold
      shutdownPi();
    }
  }
});

function powerOn() {
  console.log("powerOn");
}

function ledToggle() {
  console.log("ledToggle");
}

function nesReset() {
  console.log("nesReset");
}

function shutdownPi() {
  console.log("Shutdown Pi");
}

function eject() {
  cartok = false;
  console.log("EJECTED");
}

function insert(records) {
  var consoleName, rom;
  for(var i = 0; i < records.length; i++) {
    var record = records[i];
    var payload, contents;

    if (record.tnf === ndef.TNF_WELL_KNOWN && record.type[0] === ndef.RTD_TEXT[0]) {
      contents = ndef.text.decodePayload(record.payload);
    } else if (record.tnf === ndef.TNF_WELL_KNOWN && record.type[0] === ndef.RTD_URI[0]) {
      contents = ndef.uri.decodePayload(record.payload);
    } else {
      console.log("Unknown type.");
    }

    switch(i) {
      case 0: // console
        if(checkConsole(contents)) {
          consoleName = contents;
        }
        break;
      case 1: //rom
        if(checkRom(consoleName, contents)) {
          rom = contents;
        }
        break;
    }
  }

  if (consoleName && rom) {
    emulatorpath = getEmulatorPath(consoleName);
    rompath = getRomPath(consoleName, rom);
    cartok = true;
  }
}

function checkConsole(name) {
  const valid = emulators.includes(name);
  if (valid) {
    console.log('NDEF Record "' + name + '" is a valid system...');
  } else {
        console.log('Could not find "' + name + '" in the supported systems list');
        console.log('Check NDEF Record 1 for a valid system name(all-lowercase)');
  }
  return valid;
}

function checkRom(consoleName, rom) {
  const romfile = '/home/pi/RetroPie/roms/' + consoleName + '/' + rom;
  if (fs.existsSync(romfile)) {
    console.log('Found "' + rom + '"');
    return true;
  } else {
    console.log('But couldn\'t find "' + romfile + '"');
    console.log('Check NDEF Record 2 contains a valid filename...');
    return false;
  }
}

function getRomPath(consoleName, rom) {
  const rompath = '/home/pi/RetroPie/roms/' + consoleName + '/' + rom;
  return rompath;
}

function getEmulatorPath(consoleName) {
  return '/opt/retropie/supplementary/runcommand/runcommand.sh 0 _SYS_ ' + consoleName + ' ';
}

function buttonOn() {
  if (cartok) {
    fkill(procnames)
      .then(function() {
        return fkill(['kodi', 'kodi.bin'], {force: true});
      })
    .then(runGame).catch(killFail);
  } else {
    console.log("no valid cartridge inserted...");
  }
}

function runGame() {
  console.log("runGame", emulatorpath, rompath);
  //var game = spawn('sudo', ['openvt', '-c', '1', '-s', '-f', emulatorpath, rompath]);

}

function buttonOff() {
  ps.lookup({command: 'emulationstation'}, function(err, resultList) {
    if (err) {
      throw new Error( err );
    }
    if(resultList.length > 0) {
      console.log("emulationstation is running...");
    } else {
      fkill(procnames)
        .then(function() {
          fkill(['kodi', 'kodi.bin'], {force: true})
        }).catch(killFail);
    }
  });
}

function killFail(e) {
  console.log("Killfail");
}

